FFLAGS=-g
FOPT=
FFLAGS=-g -Wall -std=f2008ts -fimplicit-none -Wno-maybe-uninitialized -static-libgfortran -flto -fbacktrace
FOPT=-O3 -fopt-info-vec
NAME=lab2
FC=gfortran-6.4.0

all: lab2

lab2:
	$(FC) $(FFLAGS) -c src/$(NAME).f90 -J obj/ -o obj/$(NAME).o
	$(FC) $(FFLAGS) -c src/DECOMP.F -J obj/ -o obj/DECOMP.o
	$(FC) $(FFLAGS) -c src/SOLVE.F -J obj/ -o obj/SOLVE.o
	$(FC) $(FFLAGS) $(FOPT) -o bin/$(NAME) obj/SOLVE.o obj/DECOMP.o obj/$(NAME).o 

run:
	cd ./bin; ./$(NAME)