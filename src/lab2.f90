program lab1
    use ISO_Fortran_Env
    implicit none
    external DECOMP
    external SEVAL
    integer, parameter      :: i_length=4
    integer, parameter      :: a_length=100
    integer, parameter      :: n = 8
    integer(kind=i_length)  :: in_fd, out_fd
    character(*), parameter :: in_array_a="../data/array_a.dat"
    character(*), parameter :: in_array_b="../data/array_b.dat"
    integer :: i = 0
    real :: p(6) = [ 1.0, 0.1, 0.01, 0.001, 0.0001, 0.000001]
    real :: a(n,n)
    real :: at(n,n)
    real :: b(n)
    real :: cond(2,6)
    real :: xx(2, 6, n)


    open(file="../data/result_a.dat", newunit=out_fd)
    do i = 1, 6
        open (file=in_array_a, newunit=in_fd)
          read(in_fd, *) a
        close(in_fd)
        open (file=in_array_b, newunit=in_fd)
          read(in_fd, *) b
        close(in_fd)
        a(1,1) = p(i) - 29
        b(1) = p(i) * 4 - 175
        call eq1(a, b, cond(1,i), xx(1,i,:))
        write(out_fd,"(9F12.7)") p(i), xx(1,i,:)
    end do
    close(out_fd)

    open(file="../data/result_at.dat", newunit=out_fd)
    do i = 1, 6
        open (file=in_array_a, newunit=in_fd)
          read(in_fd, *) a
        close(in_fd)
        open (file=in_array_b, newunit=in_fd)
          read(in_fd, *) b
        close(in_fd)
        a(1,1) = p(i) - 29
        at = transpose(a)
        a = matmul(at, a)
        b(1) = p(i) * 4 - 175
        b = matmul(at,b)
        call eq1(a, b, cond(2,i), xx(2,i,:))
        write(out_fd,"(9F12.7)") p(i), xx(2,i,:)
    end do
    close(out_fd)
    

    open(file="../data/cond.dat", newunit=out_fd)
    do i = 1, 6
        write(out_fd, "(F8.6,1X,F10.7,1X,F11.7)") p(i), cond(:,i)
    end do
    close(out_fd)

    open(file="../data/norm.dat", newunit=out_fd)
    do i = 1, 6
        write(out_fd, "(F8.6,1X,F12.10)") p(i), f_norm(xx(1,i,:) - xx(2,i,:), 1, n) / f_norm(xx(1,i,:), 1, n)
    end do
    close(out_fd)

contains
    subroutine eq1(in_a, in_b, cond, x)
        real, intent(inout) :: cond
        real :: work(n)
        real, dimension(n,n), intent(in)  :: in_a
        real, dimension(n), intent(in)  :: in_b
        real, dimension(n), intent(out) :: x
        real :: a(n,n), b(n)
        integer :: ipvt(n)
        a = in_a
        b = in_b
        
        call DECOMP(n, n, a, cond, ipvt, work)
        call SOLVE(n, n, a, b, ipvt)
        x = b
    end subroutine eq1

    real function f_norm(a, ni, nj) result(norm)
        integer, intent(in) :: ni, nj
        real, dimension(ni, nj), intent(in) :: a
        real :: tmp
        integer :: i, j

        tmp = 0
        do i = 1, ni
            do j = 1, nj
                tmp = tmp + a(i, j) * a(i, j)
            end do
        end do
        norm = sqrt(tmp)
    end function f_norm
end program lab1